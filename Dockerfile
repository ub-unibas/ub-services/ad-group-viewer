FROM php:apache

# Install necessary packages
RUN apt-get update && \
    apt-get install -y vim libldb-dev libldap2-dev && \
    docker-php-ext-install -j$(nproc) ldap && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get update -y --fix-missing

#RUN mv /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
#RUN sed -i 's/;extension=ldap/extension=ldap/' /usr/local/etc/php/php.ini
RUN service apache2 restart

# copy scripts to webroot
COPY scripts/ /var/www/html/
