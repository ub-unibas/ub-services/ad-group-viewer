<?php

/**
 * show-ad-info.php:  Anzeige von Gruppen und Gruppenzugehoerigkeiten
 * (fuer iFrame im Intranet)
 * 
 * author:
 *  andres.vonarx@unibas.ch
 * 
 * history;
 *  01.10.2015 andres.vonarx@unibas.ch
 **/

    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    $TO_UTF8 = true;                        # if true, convert to UTF-8
    $LDAP_TLS = false;                      # if true, start LDAP TLS

    @$GID = $_REQUEST['gid'];
    @$UID = $_REQUEST['uid'];
    $SHOWALL = ( @$_REQUEST['showall'] == 'on' ) ? TRUE : FALSE;
    if ( $UID == '' ) {
        @$UID=$_SERVER["AUTHORIZE_UID"];
    }
    
    $LDAP_URL           = 'ldaps://unibasel.ads.unibas.ch';
    $LDAP_UID           = 'unibasel\ub-svc-scrpt';
    $LDAP_PWD           =  getenv("LDAP_PWD");
    $group_basedn       = 'OU=rights-groups,OU=Groups,OU=UB,OU=ITSC-PG2,DC=unibasel,DC=ads,DC=unibas,DC=ch';
    $group_filter       = '(objectClass=group)';
    $group_attrib       = array('cn','member','description');
        $user_basedn    = 'DC=unibasel,DC=ads,DC=unibas,DC=ch';
        #$user_basedn    = ' ':
        $user_filter    = '(&(uid=%%UID%%)(objectClass=user))';
        $user_attrib    = array('memberof');
    
    # -- connection
    $LDAP=ldap_connect($LDAP_URL);
    if ( ! $LDAP ) {
        die("LDAP ERROR: cannot connect");
    }
    if ( $LDAP_TLS ) {
                if ( ! @ldap_start_tls($LDAP) ) {
                        die("LADP ERROR: cannot start TLS: " .ldap_error($LDAP));
                }
        }
        ldap_set_option($LDAP, LDAP_OPT_REFERRALS, 0);
        ldap_set_option($LDAP, LDAP_OPT_PROTOCOL_VERSION, 3);

        if ( ! @ldap_bind($LDAP, $LDAP_UID, $LDAP_PWD)) {
                die("LDAP ERROR: cannot bind");
        }

        # $UBGROUPS = Liste der "UB rights-groups".
        #       Struktur:
        #               ["name"]->[description] 
        #               ["name"]->[members]
        #
    $sr=ldap_search($LDAP, $group_basedn, $group_filter, $group_attrib); 
    $a = ldap_get_entries($LDAP, $sr);
    
    $cnt = $a['count'];
    for ( $i=0 ; $i < $cnt ; $i++ ) {
                @$cn = $a["$i"]['cn']['0'];
                @$desc = $a["$i"]['description']['0'];
                $desc = preg_replace('/[\n\t]/',' ', $desc);
                if ( $TO_UTF8 ) {
                        $desc=iconv('WINDOWS-1252','utf-8',$desc);
                }
                $members=array();
                @$b=$a["$i"]["member"];
                if ( is_array($b) ) {
                        unset($b['count']);
                        $search=array('/^CN=/','/\,.*$/');
                        $replace=array('','');
                        foreach ( $b as $member ) {
                                $member_is_group='';
                                if ( preg_match('/OU=Group/',$member) ) {
                                        $member_is_group= ' <span class="subgroup">[= Gruppe bestehend aus:]</span>';
                                }
                                $member=preg_replace($search,$replace,$member);
                                if ( $TO_UTF8 ) {
                                        $member=iconv('WINDOWS-1252','utf-8',$member);
                                }
                                array_push($members,$member .$member_is_group);
                        }
                        sort($members);
                }
                $UBGROUPS["$cn"]["description"]=$desc;
                $UBGROUPS["$cn"]["member"]=$members;
        }

        # $MEMBEROF = Liste der Gruppen-Mitgliedschaft von user $UID
        #       Struktur: array mit "cn" der Gruppenbezeichnung
        # 
        $MEMBEROF=array();
    if ( $UID ) {
                $UID = preg_replace('/\W/','',$UID);    # security
                $user_filter=preg_replace('/%%UID%%/',$UID,$user_filter);
                $sr=ldap_search($LDAP, $user_basedn, $user_filter, $user_attrib); 
                $a=ldap_get_entries($LDAP, $sr);
                @$arr = $a['0']['memberof'];
                if ( is_array($arr) ) {
                        unset($arr['count']);
                        $suche=array( '/^CN=/', '/\,.*$/' );
                        $ersetze=array('','');
                        foreach( $arr as $group ) {
                                $group = preg_replace($suche, $ersetze, $group);
                                if ( $SHOWALL == FALSE ) {
                                        if ( ! preg_match('/^UB-/', $group) ) {
                                                continue;
                                        }
                                }
                                if ( $TO_UTF8 ) {
                                        $group = iconv('WINDOWS-1252','utf-8',$group);
                                }
                                array_push($MEMBEROF,$group);
                        }
                } else {
            $MEMBEROF[0] = "Benutzer '$UID' ist im Active Directory der Uni Basel "
                ."nicht als persönlicher Benutzer im Bereich UB registriert.";
        }
        }
    ldap_close($LDAP);
    
    ksort($UBGROUPS);
        sort($MEMBEROF);

        function expand_and_print_members($gid,$indent='') {
                global $UBGROUPS;
                @$members=$UBGROUPS[$gid]['member'];
                foreach ( $members as $member ) {
                        print $indent .$member ."\n";
                        if ( preg_match('/</',$member) ) {
                                $nested_group=preg_replace('/ *<.*$/','',$member);
                                expand_and_print_members($nested_group, $indent .'    ');
                        }
                }
        }

////////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
<title>UB Basel: AD</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="http://www.ub.unibas.ch/css/CgiCms.css" />
<style type="text/css">
/*<![CDATA[*/

body  { padding-left:5mm; font-family:sans-serif; font-size:80%;}
pre   { font-size:120%; }
table { border-collapse:collapse; empty-cells:show; }
th    { text-align:left; }
hr    { border:0; height:1px; color:#000; background-color:#000; }
span.subgroup           { color:#77B; font-style:italic; }
#content form label     { display:inline-block; width:100px; }
#content form input     { width:350px; }
#content form select    { width:350px;}
#content form fieldset  { background-color:#F8F8F8; }
#content form input[type=submit] { width: 40px; }
#content form input[type=checkbox] { width: 20px; }

/*]]>*/
</style>
</head>
<body onLoad="if ( parent.window.scrollTo ) parent.window.scrollTo(0,0);">
<div id="content">

<h3>In welchen Gruppen bin ich?</h3>
<form method="post" name="form1" action="show-ad-info.php">
<fieldset>
<label for="iduid">AD Benutzer:</label>
<input name="uid" id="iduid" type="text" value="<?php print $UID ?>"/>
<input type="submit" id="idb1" value="&gt;&gt;" name="b1"/>
<input type="checkbox" id="idshowall" name="showall" value="on" 
<?php
    if ( $SHOWALL ) {
        print ' checked="checked"';
    }
?> title="alle Gruppen-Mitgliedschaften anzeigen (auch Gruppen ausserhalb der UB)"/> alle
</fieldset>
</form>


<h3>Wer gehört zu welcher Gruppe?</h3>
<form method="post" name="form2" action="show-ad-info.php">
<fieldset>
<label for="idgid">AD Gruppe:</label>
<select name="gid" id="idgid">
<?php

        foreach ( $UBGROUPS as $key => $val ) {
                $selected='';
                if ( $key == $GID ) {
                        $selected = 'selected="selected"';
                }
                print "<option value=\"$key\" $selected>$key</option>\n";
        }
?>
</select>
<input type="submit" id="idb2" value="&gt;&gt;" name="b2"/>
</fieldset>
</form>
<?php

    if ( $GID != '' ) {
        print "<h3>Gruppe '$GID' hat folgende Mitglieder:</h3>\n";
        print "<pre>";
        expand_and_print_members($GID);
                print "</pre>";
        } else {
        print <<<EOD
<h3>BenutzerIn '$UID' ist in folgenden Gruppen:</h3>
<table border="1" cellpadding="3" width="100%">
<tr><th>Gruppe</th><th>Kommentar</th></tr>

EOD;
                foreach ( $MEMBEROF as $group ) {
                        @$desc = $UBGROUPS[$group]['description'];
                        print "<tr><td>$group</td><td>$desc</td></tr>\n";
                }
        print "</table>\n";
    }
?>
<hr style="margin-top:5mm" />
<a href="show-ad-info-help.html" alt="Hilfe"><img src="pic/InfoLogo.png" style="height:25px; width:25px" /></a>

Stand: <?php print date("Y-m-d H:i:s");?>


</div>
</body>
</html>
